# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi
require python [ blacklist=2 multiunpack=true ]

SUMMARY="Simplified packaging of Python modules"
DESCRIPTION="
Flit is a simple way to put Python packages and modules on PyPI. It tries to
require less thought about packaging and help you avoid common mistakes."

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/tomli[python_abis:*(-)?]
    suggestion:
        dev-python/testpath[python_abis:*(-)?]
        dev-python/pytest[python_abis:*(-)?]
"

prepare_one_multibuild() {
    python_prepare_one_multibuild

    # Remove vendored tomli, it can handle that, see flit_core/config.py
    edo rm -r ${PN}/vendor/tomli
}

compile_one_multibuild() {
    edo ${PYTHON} -m flit_core.wheel
}

test_one_multibuild() {
    if has_version dev-python/testpath[python_abis:$(python_get_abi)] && \
       has_version dev-python/pytest[python_abis:$(python_get_abi)] ; then
        PYTHONPATH="." edo ${PYTHON} -m pytest
    else
        ewarn "Test dependencies are not yet installed (due to cyclic"
        ewarn "dependencies), skipping tests"
    fi
}

install_one_multibuild() {
    edo ${PYTHON} bootstrap_install.py --install-root "${IMAGE}" dist/${PNV}-py3-none-any.whl

    python_bytecompile

    emagicdocs
}

