# Copyright 2023 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=samuelcolvin tag=v${PV} ]
require py-pep517 [ backend=hatchling test=pytest ]

SUMMARY="Doing dirty (but extremely useful) things with equals"
DESCRIPTION="
dirty-equals is a python library that (mis)uses the __eq__ method to make
python code (generally unit tests) more declarative and therefore easier to
read and write.
dirty-equals can be used in whatever context you like, but it comes into its
own when writing unit tests for applications where you're commonly checking
the response to API calls and the contents of a database."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/pytz[>=2021.3][python_abis:*(-)?]
    test:
        dev-python/packaging[python_abis:*(-)?]
"

PYTEST_SKIP=(
    # "this requires the system timezone to be UTC"
    test_is_datetime[unix-int]
    test_is_datetime[unix-float]
    test_docs_examples[dirty_equals/_datetime.py
)

