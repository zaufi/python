# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'attrs-16.3.0-r1.ebuild' from Gentoo, which is:
#   Copyright 1999-2017 Gentoo Foundation

require pypi py-pep517 [ backend=setuptools python_opts=[sqlite] ]

SUMMARY="Attributes without boilerplate"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

# pytest>=4.3.0 is listed, but works fine with 3.10
DEPENDENCIES="
    run+test:
        dev-python/zopeinterface[python_abis:*(-)?]
    test:
        dev-python/coverage[>=5.0.2][python_abis:*(-)?]
        dev-python/Pympler[python_abis:*(-)?]
        dev-python/zopeinterface[python_abis:*(-)?]
        dev-python/mypy[>=1.1.1][python_abis:*(-)?]
    suggestion:
        dev-python/hypothesis[>=3.6.0][python_abis:*(-)?] [[ description = [ required for the test suite ] ]]
        dev-python/pytest[python_abis:*(-)?] [[ description = [ required for the test suite ] ]]
        dev-python/pytest-mypy-plugins[python_abis:*(-)?] [[ description = [ required for the test suite ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Update-Mypy-tests-for-new-Mypy-output-1070.patch
    "${FILES}"/${PNV}-Fix-for-new-Mypy-1111.patch
)

_test_dependencies_satisfied() {
    has_version "dev-python/hypothesis[>=3.6.0][python_abis:$(python_get_abi)]" || return 1
    has_version "dev-python/pytest[python_abis:$(python_get_abi)]" || return 1
    has_version "dev-python/pytest-mypy-plugins[python_abis:$(python_get_abi)]" || return 1
}

test_one_multibuild() {
    # avoid a attrs <-> hypothesis <--> pytest/pytest-mypy-plugins dependency loop
    if _test_dependencies_satisfied; then
        # From setup-py.exlib, can't use setup-py_test_one_multibuild, that
        # adds a dependency to dev-python/pytest itself
        PYTEST="py.test-$(python_get_abi)"

        PYTHONPATH="$(ls -d ${PWD}/build/lib*)" edo ${PYTEST} "${PYTEST_PARAMS[@]}"
    else
        ewarn "One or more test dependencies not yet installed, skipping tests"
    fi
}

