# Copyright 2022 Ridai Govinda Pombo <ridai.govinda@keemail.me>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=setuptools test=pytest entrypoints=[ ${PN} ] ]

SUMMARY="A 7z library on Python"
DESCRIPTION="
7zip in python3 with ZStandard, PPMd, LZMA2, LZMA1, Delta, BCJ, BZip2, and Deflate compressions, and AES encryption.
"
HOMEPAGE="https://github.com/miurahr/py7zr"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-python/setuptools_scm[>=7.0.5][python_abis:*(-)?]
    build+run:
        app-arch/brotli[>=1.0.9][python][python_abis:*(-)?]
        dev-python/inflate64[>=0.3.1][python_abis:*(-)?]
        dev-python/multivolumefile[>=0.2.3][python_abis:*(-)?]
        dev-python/psutil[python_abis:*(-)?]
        dev-python/pybcj[>=0.6.0][python_abis:*(-)?]
        dev-python/pycryptodomex[>=3.6.6][python_abis:*(-)?]
        dev-python/pyppmd[>=0.18.1&<1.1.0][python_abis:*(-)?]
        dev-python/pyzstd[>=0.14.4][python_abis:*(-)?]
        dev-python/texttable[python_abis:*(-)?]
    test:
        dev-python/pyannotate[python_abis:*(-)?]
        dev-python/py-cpuinfo[python_abis:*(-)?]
        dev-python/pytest-benchmark[python_abis:*(-)?]
        dev-python/pytest-cov[python_abis:*(-)?]
        dev-python/pytest-timeout[python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/noexit-sys.exit.instead.patch
)

PYTEST_PARAMS=( --ignore=tests/test_concurrent.py )

