# Copyright 2020-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi [ pnv=${PNV/-/_} ] setup-py [ import=setuptools blacklist='2' test=pytest work=${PNV/-/_} ]

SUMMARY="Library to communicate with remote servers over GMP or OSP"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/defusedxml[>=0.6.0&<0.8][python_abis:*(-)?]
        dev-python/lxml[>=4.5.0&<5.0.0][python_abis:*(-)?]
        dev-python/paramiko[>=2.7.1&<4.0.0][python_abis:*(-)?]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-23.2.0-paramiko.patch
)

PYTEST_PARAMS=(
    -k "not SSHConnectionTestCase"
)

test_one_multibuild() {
    esandbox allow_net "unix:${TEMP%/}/*.sock"

    setup-py_test_one_multibuild

    esandbox disallow_net "unix:${TEMP%/}/*.sock"
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # do not install test stuff into site-packages
    edo rm -rf "${IMAGE}"$(python_get_libdir)/site-packages/{setup.py,__pycache__,tests}
}

