# Copyright 2019-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'PyQt5.exlib', which is:
#     Copyright 2008-2016 Wulf C. Krueger <philantrop@exherbo.org>
#     Copyright 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>

myexparam sip_version

require pypi
require python [ blacklist=2 multiunpack=true ]

SUMMARY="PyQt5 is a set of Python bindings for the Qt WebEngine framework"
DESCRIPTION="The framework provides the ability to embed web content in
applications and is based on the Chrome browser. The bindings sit on top of
PyQt5 and are implemented as three separate modules corresponding to the
different libraries that make up the framework."

BASE_URI="https://www.riverbankcomputing.com"
HOMEPAGE="${BASE_URI}/software/pyqt/download5/"
DOWNLOADS="https://pypi.python.org/packages/source/P/${PN}/${PNV}.tar.gz"

SLOT="0"
LICENCES="GPL-3"
MYOPTIONS="debug"

DEPENDENCIES="
    build:
        dev-python/PyQt-builder[>=1.6][python_abis:*(-)?]
    build+run:
        dev-python/PyQt5[>=${PV}][python_abis:*(-)?][webchannel]
        dev-python/sip:0[>=$(exparam sip_version)][python_abis:*(-)?]
        x11-libs/qtbase:5
        x11-libs/qtwebengine:5
       !dev-python/PyQt5[<5.12][python_abis:*(-)?][webengine] [[
            description = [ PyQtWebEngine was split out from PyQt5 ]
            resolution = [ uninstall-blocked-after ]
        ]]
"

DEFAULT_SRC_INSTALL_PARAMS=( INSTALL_ROOT="${IMAGE}" )

prepare_one_multibuild() {
    python_prepare_one_multibuild

    # When system python is set to 2.6 python_bytecompile() can't process
    # source files that use python 3 syntax. See Gentoo bug #274499.
    [[ $(python_get_abi) == 3.* ]] && edo rm -rf pyuic/uic/port_v2
}

configure_one_multibuild() {
    local myparams=(
        --no-make
        --qmake /usr/$(exhost --target)/lib/qt5/bin/qmake
        --target-dir $(python_get_sitedir) \
        --verbose
        $(option debug && echo '--debug')
    )

    edo ${PYTHON} /usr/$(exhost --target)/bin/sip-build "${myparams[@]}"
}

compile_one_multibuild() {
    edo cd build
    emake
}

install_one_multibuild() {
    edo pushd build

    default
    python_bytecompile

    edo popd

    emagicdocs
}

