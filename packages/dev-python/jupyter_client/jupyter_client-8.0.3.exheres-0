# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require pypi
require py-pep517 [ backend=hatchling test=pytest \
    entrypoints=[ jupyter-{kernel,kernelspec,run} ] ]

SUMMARY="Jupyter protocol implementation and client libraries"
HOMEPAGE+=" https://jupyter.org"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/jupyter_core[>=5.1][python_abis:*(-)?]
        dev-python/python-dateutil[>=2.8.2][python_abis:*(-)?]
        dev-python/pyzmq[>=23.0][python_abis:*(-)?]
        dev-python/tornado[>=6.2][python_abis:*(-)?]
        dev-python/traitlets[>=5.3][python_abis:*(-)?]
    test:
        dev-python/pytest-jupyter[python_abis:*(-)?]
    suggestion:
        (
            dev-python/ipykernel[python_abis:*(-)?]
            dev-python/ipython[python_abis:*(-)?]
        ) [[ *description = [ Moved test dependency to break cycle ] ]]
"

# Requires unpackaged: pytest-jupyter
RESTRICT="test"

_are_test_deps_installed() {
    has_version "dev-python/ipython[python_abis:$(python_get_abi)]" || return 1
    has_version "dev-python/ipykernel[python_abis:$(python_get_abi)]" || return 1
}

test_one_multibuild() {
    if _are_test_deps_installed; then
        esandbox allow_net "unix:test-*"
        py-pep517_test_one_multibuild
        esandbox disallow_net "unix:test-*"
    else
        ewarn "Test dependencies are not installed yet, skip tests"
    fi
}

