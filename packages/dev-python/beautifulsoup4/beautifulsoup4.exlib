# Copyright 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'BeautifulSoup.exlib' in Exherbo, which is:
#   Copyright 2008-2012 Wulf C. Krueger <philantrop@exherbo.org>
#   Distributed under the terms of the GNU General Public License v2
#   Based in part upon 'beautifulsoup-3.0.7.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require pypi

if ever at_least 4.10.0; then
    require setup-py [ import=setuptools blacklist=2 test=pytest ]
else
    require setup-py [ import=setuptools test=nose ]
fi

SUMMARY="Screen-scraping library"
DESCRIPTION="
Beautiful Soup sits atop an HTML or XML parser, providing Pythonic idioms for iterating,
searching, and modifying the parse tree.
"
HOMEPAGE+=" https://www.crummy.com/software/BeautifulSoup"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/doc [[ lang = en ]]"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="doc [[ description = [ Install HTML documentation ] ]]"

DEPENDENCIES="
    build:
        doc? ( dev-python/Sphinx[>=2.1.2] )
    build+run:
        dev-python/soupsieve[>=1.2][python_abis:*(-)?]
    suggestion:
        dev-python/html5lib[python_abis:*(-)?] [[
            description = [ Use html5lib instead of HTMLParser for faster parsing ]
        ]]
        dev-python/lxml[python_abis:*(-)?] [[
            description = [ Use lxml instead of HTMLParser for fastest parsing ]
        ]]
"

if ! ever at_least 4.10.0; then
    DEPENDENCIES+="
        build+run:
            python_abis:2.7? ( dev-python/soupsieve[>=1.2&<2.0][python_abis:2.7] )
    "
    # point to refactored (2to3) tests
    NOSETESTS_PARAMS=(
        "build/lib"
    )
fi

DEFAULT_SRC_INSTALL_EXCLUDE=( PKG-INFO )

compile_one_multibuild() {
    SETUP_PY_SRC_COMPILE_PARAMS=(
        $(option doc build_sphinx)
    )
    setup-py_compile_one_multibuild
}

install_one_multibuild() {
    setup-py_install_one_multibuild

    # Install HTML docs excluding intersphinx file
    if option doc; then
        edo pushd build/sphinx
        edo rm html/objects.inv
        dodoc -r html
        edo popd
    fi
}

